import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StudentService } from 'src/app/services/student.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { CourseService } from 'src/app/services/course.service';
import { AsignedCourseService } from 'src/app/services/asignedCourse.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-student-modal',
  templateUrl: './student-modal.component.html',
  styleUrls: ['./student-modal.component.scss']
})
export class StudentModalComponent implements OnInit {

  displayedColumns: string[] = ['#', 'Nombre', 'N Creditos', 'Accion'];

  form!: FormGroup;

  modeModalCreate: boolean = true;

  updatedId: string = '';

  isAsignedCourse: boolean = false;

  courses: any = [];

  myCourses: any = [];

  asignedCourseData: any = {
    IdEstudiante: 0,
    IdCurso: 0,
    NotaFinal: 0
  }

  constructor(
    public dialogRef: MatDialogRef<StudentModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private studentService: StudentService,
    private courseService: CourseService,
    private asignedCourseService: AsignedCourseService,
    private _snackBar: MatSnackBar) {
      this.buildForm();
      this.isUpdate();
  }

  ngOnInit(): void { }

  isUpdate() {
    if (this.data !== null) {
      this.modeModalCreate = false;
      this.buildFormUpdate();
      this.getAllCourses();
      this.getMyCourses();
    }
  }

  asignedCourse(idCourse: any){
    this.asignedCourseData.IdEstudiante = this.data;
    this.asignedCourseData.IdCurso = idCourse;
    this.asignedCourseService.asignedCourse(this.asignedCourseData).subscribe((data) => {
      this.getMyCourses();
      this.getAllCourses();
    }, err => {
      console.log(err.error.error.message)
    })
  }

  deleteAsignedCourse(courseId: any){
    this.asignedCourseService.deleteAsignedCourse(this.data, courseId).subscribe((data) => {
      this.getMyCourses();
      this.getAllCourses();
    });
  }

  goAsignedCourse() {
    this.getAllCourses();
    this.getMyCourses();
    this.isAsignedCourse = true;
  }

  goUpdatedStudent() {
    this.isAsignedCourse = false;
  }

  getAllCourses() {
    this.courseService.getNotAsignedCourse(this.data).subscribe((data) => {
      this.courses = data;
    })
  }
  
  getMyCourses() {
    this.courseService.getAsignedCourse(this.data).subscribe((data) => {
      this.myCourses = data;
    })
  }

  buildFormUpdate() {
    this.studentService.getStudent(this.data).subscribe((student) => {
      this.form.patchValue({
        Id: student[0].Id,
        TipoIdentificacion: student[0].TipoIdentificacion,
        Identificacion: student[0].Identificacion,
        Nombre1: student[0].Nombre1,
        Nombre2: student[0].Nombre2,
        Apellido1: student[0].Apellido1,
        Apellido2: student[0].Apellido2,
        Email: student[0].Email,
        Celular: student[0].Celular,
        Direccion: student[0].Direccion,
        Ciudad: student[0].Ciudad
      })
    })
  }

  saveOrUpdateStudent(event: Event) {
    event.preventDefault();
    if (this.modeModalCreate) {
      this.saveStudent();
    } else {
      this.updateStudent();
    }
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, '', {
      duration: 3000,
    });
  }

  saveStudent() {
    if (this.form.valid) {
      let student: any = this.form.value;
      delete student.Id;
      (student.Nombre2 === '') ? delete student.Nombre2 : student.Nombre2 = student.Nombre2;
      (student.Apellido2 === '') ? delete student.Apellido2 : student.Apellido2 = student.Apellido2;
      (student.Celular === '') ? delete student.Celular : student.Celular = student.Celular;
      (student.Direccion === '') ? delete student.Direccion : student.Direccion = student.Direccion;

      this.studentService.saveStudent(student).subscribe((newStudent: any) => {
        this.form.patchValue({ Id: newStudent.Id })
        this.data = newStudent.Id;
        this.modeModalCreate = false;
        this.openSnackBar('Se creo correctamente el estudiante.');
      })
    }
  }

  updateStudent() {
    if (this.form.valid) {
      let student: any = this.form.value;
      (student.Nombre2 === '') ? delete student.Nombre2 : student.Nombre2 = student.Nombre2;
      (student.Apellido2 === '') ? delete student.Apellido2 : student.Apellido2 = student.Apellido2;
      (student.Celular === '') ? delete student.Celular : student.Celular = student.Celular;
      (student.Direccion === '') ? delete student.Direccion : student.Direccion = student.Direccion;

      this.studentService.updateStudent(student, this.data).subscribe((updateStudent: any) => {
        this.modeModalCreate = false;
        this.openSnackBar('Se actualizo correctamente el estudiante.');
      })
    }
  }

  private buildForm() {
    this.form = this.fb.group({
      Id: [''],
      TipoIdentificacion: ['', Validators.required],
      Identificacion: ['', Validators.required],
      Nombre1: ['', Validators.required],
      Nombre2: ['',],
      Apellido1: ['', Validators.required],
      Apellido2: ['',],
      Email: [''],
      Celular: ['',],
      Direccion: ['',],
      Ciudad: ['', Validators.required,],
    })
  }

}
