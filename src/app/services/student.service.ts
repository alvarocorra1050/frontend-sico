import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

const { MICROSERVICE_URL } = environment;

@Injectable({
    providedIn: 'root'
})
export class StudentService {

    url = `${MICROSERVICE_URL}/api/backend-sico`;
    constructor(private http: HttpClient) { }

    getStudent(id: string) {
        return this.http.get<any>(this.url + '/students/' + id)
    }
    
    getAllStudents() {
        return this.http.get<any>(this.url + '/students', {headers: {'Access-Control-Allow-Origin': '*'}})
    }

    saveStudent(postData: any) {
        return this.http.post(this.url + '/students', postData);
    }
    
    updateStudent(changes: any, id: string) {
        return this.http.put(this.url + '/students/' + id, changes);
    }
    
    deleteStudent(id: String) {
        return this.http.delete(this.url + '/students/' + id);
    }

    /*editCategory(postData: any) {
        postData.append('token', this.helperService.generarToken());
        return this.http.post(this.url, postData);
    }

    deleteCategory(id: any) {
        return this.http.delete(this.url + '?action=delete&id=' + id + '&token=' +
        this.helperService.generarToken());
    }*/
}
