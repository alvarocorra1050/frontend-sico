import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

const { MICROSERVICE_URL } = environment;

@Injectable({
    providedIn: 'root'
})
export class AsignedCourseService {

    url = `${MICROSERVICE_URL}/api/backend-sico`;
    constructor(private http: HttpClient) { }

    asignedCourse(studentCourse: any) {
        return this.http.post<any>(this.url + '/student/course', studentCourse);
    }

    deleteAsignedCourse(studentId: string, courseId: string) {
        return this.http.delete<any>(this.url + '/student/course/' + studentId + '/' + courseId);
    }
}
