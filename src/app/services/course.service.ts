import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

const { MICROSERVICE_URL } = environment;

@Injectable({
    providedIn: 'root'
})
export class CourseService {

    url = `${MICROSERVICE_URL}/api/backend-sico`;
    constructor(private http: HttpClient) { }

    getAllCourses() {
        return this.http.get<any>(this.url + '/courses')
    }

    getAsignedCourse(id: string) {
        return this.http.get<any>(this.url + '/student/course/' + id)
    }

    getNotAsignedCourse(id: string) {
        return this.http.get<any>(this.url + '/student/not-course/' + id)
    }
}
