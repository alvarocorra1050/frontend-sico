import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { StudentModalComponent } from '../modals/student/student-modal.component';
import { StudentService } from 'src/app/services/student.service';
import { FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table'; 
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {

  students: any = []
  displayedColumns: string[] = ['#', 'Tipo identificación', 'Identificación','Nombre1','Apellido1','Celular','Ciudad','Email', 'Accion'];
  dataSource = new MatTableDataSource();

  idFilter = new FormControl('');
  name1Filter = new FormControl('');
  lastName1Filter = new FormControl('');
  emailFilter = new FormControl('');

  filterValues = {
    Identificacion: '',
    Nombre1: '',
    Apellido1: '',
    Email: ''
  };

  constructor(public dialog: MatDialog, private studentService: StudentService, private _snackBar: MatSnackBar) {
    this.getStudents();
    this.dataSource.filterPredicate = this.createFilter();
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, '', {
      duration: 3000,
    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(StudentModalComponent);
    
    dialogRef.afterClosed().subscribe(result => {
      this.getStudents();
      console.log(`Dialog result: ${result}`);
    });
  }

  openDialogUpdate(id: string) {
    const dialogRef = this.dialog.open(StudentModalComponent, { data: id });
    dialogRef.afterClosed().subscribe(result => {
      this.getStudents();
      console.log(`Dialog result: ${result}`);
    });
  }

  ngOnInit(): void {
    this.buildFilters();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteStudent(id: String){
    this.studentService.deleteStudent(id).subscribe((data) => {
      this.getStudents();
      this.openSnackBar('Se elimino correctamente el estudiante');
    }, error => {
      console.log(error.error.error.message);
      this.openSnackBar(error.error.error.message);
    })
  }

  getStudents() {
    this.studentService.getAllStudents().subscribe(data => {
      this.students = data;
      this.dataSource.data = data;
    });
  }

  buildFilters(){
    this.idFilter.valueChanges
      .subscribe(
        Identificacion => {
          this.filterValues.Identificacion = Identificacion;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.name1Filter.valueChanges
      .subscribe(
        Nombre1 => {
          this.filterValues.Nombre1 = Nombre1;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.lastName1Filter.valueChanges
      .subscribe(
        Apellido1 => {
          this.filterValues.Apellido1 = Apellido1;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.emailFilter.valueChanges
      .subscribe(
        Email => {
          this.filterValues.Email = Email;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
  }

  createFilter(): (data: any, filter: any) => boolean {
    let filterFunction = function(data: any, filter: string): boolean {
      let searchTerms = JSON.parse(filter);
      return data.Identificacion.toLowerCase().indexOf(searchTerms.Identificacion.toLowerCase()) !== -1
        && data.Nombre1.toString().toLowerCase().indexOf(searchTerms.Nombre1.toLowerCase()) !== -1
        && data.Apellido1.toLowerCase().indexOf(searchTerms.Apellido1.toLowerCase()) !== -1
        && data.Email.toLowerCase().indexOf(searchTerms.Email.toLowerCase()) !== -1;
    }
    return filterFunction;
  }
}
